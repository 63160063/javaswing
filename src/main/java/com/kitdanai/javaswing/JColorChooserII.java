/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kitdanai.javaswing;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JTextArea;

/**
 *
 * @author ADMIN
 */
public class JColorChooserII extends JFrame implements ActionListener {

    JFrame f;
    JButton b;
    JTextArea ta;

    JColorChooserII() {
        f = new JFrame("Color Chooser Example.");
        b = new JButton("Pad Color");
        b.setBounds(200, 250, 100, 30);
        ta = new JTextArea();
        ta.setBounds(10, 10, 300, 200);
        b.addActionListener(this);
        f.add(b);
        f.add(ta);
        f.setLayout(null);
        f.setSize(400, 400);
        f.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
         Color c=JColorChooser.showDialog(this,"Choose",Color.CYAN);  
    ta.setBackground(c); 
    }
    public static void main(String[] args) {
        new JColorChooserII();
    }
}
